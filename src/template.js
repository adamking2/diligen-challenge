module.exports = `
<html>
  <head>
    <title>Documents</title>
    <link href="/styles.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div class="page">
      <div id="text">
        %text%
      </div>
    </div>
    <script src="/bundle.js"></script>
  </body>
</html>
`;
