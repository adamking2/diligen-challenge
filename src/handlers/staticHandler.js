const fs = require('fs');
const path = require('path');

const publicPath = path.join(__dirname, '../../public');

function staticHandler(request, response) {
  const filePath = request.url;
  const fullPath = path.join(publicPath, filePath);

  if (fs.existsSync(fullPath) && fs.statSync(fullPath).isFile()) {
    response.setHeader('Content-Type', getMimeType(fullPath));
    response.end(fs.readFileSync(fullPath));
    return true;
  } else {
    return false;
  }
}

function getMimeType(filePath) {
  const ext = path.extname(filePath);

  switch (ext) {
    case '.js':
      return 'text/javascript';
    case '.css':
      return 'text/css';
    default:
      return 'text/plain';
  }
}

module.exports = staticHandler;
