// https://en.wikipedia.org/wiki/Basic_access_authentication
function basicAuthMiddleware(request, response) {
  const authHeader = request.headers.authorization;

  const exit = () => {
    sendChallenge(response);
    return true;
  }

  if (!authHeader) {
    return exit();
  }

  const space = authHeader.indexOf(' ');

  if (space == -1) {
    return exit();
  }

  const authType = authHeader.substring(0, space);
  const credentials = authHeader.substring(space + 1);

  if (authType !== 'Basic') {
    return exit();
  }

  const decodedCred = new Buffer(credentials, 'base64').toString('ascii');
  const colon = decodedCred.indexOf(':');

  if (colon == -1) {
    return exit();
  }

  const username = decodedCred.substring(0, colon);
  const password = decodedCred.substring(colon + 1);

  if (username !== 'diligen' || password !== 'highlight') {
    return exit();
  }

  return false;
}

function sendChallenge(response) {
  response.statusCode = 401;
  response.setHeader('WWW-Authenticate', 'Basic realm="Diligen Challenge"');
  response.end('Unauthorized');
}

module.exports = basicAuthMiddleware;
