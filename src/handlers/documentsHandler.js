const data = require('../data');
const template = require('../template');

function escapeHTML(string) {
    return string.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
}

function documentsHandler(request, response) {
  const match = request.url.match(/^\/documents\/([^\/]*)$/);

  if (match == null) {
    return false;
  }

  const index = parseInt(match[1]);

  if (isNaN(index) || index < 0 || index >= data.documents.length) {
    return false;
  }

  const text = data.documents[index];
  const html = template.replace('%text%', escapeHTML(text));

  response.end(html);
  return true;
}

module.exports = documentsHandler;
