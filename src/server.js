const http = require('http');

const basicAuthMiddleware = require('./handlers/basicAuthMiddleware');
const documentsHandler = require('./handlers/documentsHandler');
const staticHandler = require('./handlers/staticHandler');

const handlers = [
  // Auth middleware comes first.
  basicAuthMiddleware,
  documentsHandler,
  staticHandler
];

const requestHandler = (request, response) => {
  for (let handler of handlers) {
    const handled = handler(request, response);

    // Handlers return true if they've fully handled the request.
    if (handled) {
      return;
    }
  }

  // 404 if no handler handles the request.
  response.statusCode = 404;
  response.end('Page Not Found');
}

const server = http.createServer(requestHandler);
module.exports = server;
