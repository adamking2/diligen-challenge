/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var textElement = document.getElementById('text');
var documentText = textElement.innerText;
// Remove leading whitespace from the node.
textElement.innerHTML = textElement.innerText;

function updateSelection() {
  var selection = window.getSelection();

  if (selection.rangeCount == 0) {
    return;
  }

  var range = selection.getRangeAt(0);

  var _getOffsetsRelativeTo = getOffsetsRelativeToParent(range, textElement),
      startOffset = _getOffsetsRelativeTo.startOffset,
      endOffset = _getOffsetsRelativeTo.endOffset;

  var selectedText = selection.toString();

  var highlighted = highlightWords(documentText, selectedText);

  textElement.innerHTML = highlighted;

  // Setting innerHTML removes the selection. Restore it.
  selection.removeAllRanges();
  selection.addRange(getParentRangeFromOffsets(textElement, startOffset, endOffset));
}

function highlightWords(text, word) {
  // Make the highlighting case-insensitive.
  var lowerText = text.toLowerCase();
  word = word.toLowerCase();

  if (word.length == 0) {
    return text;
  }

  var highlighted = '';
  var pos = 0;

  while (pos < text.length) {
    var next = lowerText.indexOf(word, pos);

    if (next == -1) {
      highlighted += text.substr(pos);
      break;
    }

    highlighted += text.substr(pos, next - pos);
    highlighted += '<span class="highlighted">' + text.substr(next, word.length) + '<\/span>';
    pos = next + word.length;
  }

  return highlighted;
}

function getOffsetsRelativeToParent(range, parent) {
  var rangeBefore = document.createRange();
  rangeBefore.selectNodeContents(parent);
  rangeBefore.setEnd(range.startContainer, range.startOffset);
  var startOffset = rangeBefore.toString().length;
  var endOffset = startOffset + range.toString().length;

  return { startOffset: startOffset, endOffset: endOffset };
}

function getParentRangeFromOffsets(parent, startOffset, endOffset) {
  var range = document.createRange();
  var start = getNodeAndOffsetByParentOffset(parent, startOffset);
  var end = getNodeAndOffsetByParentOffset(parent, endOffset);
  range.setStart(start.node, start.offset);
  range.setEnd(end.node, end.offset);

  return range;
}

function getNodeAndOffsetByParentOffset(parent, offset) {
  var count = offset;

  // Count the number of characters in each child node until we find the node
  // corresponding to our offset.
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = parent.childNodes[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var child = _step.value;

      var length = child.textContent.length;

      if (count < length) {
        return {
          node: child,
          offset: count
        };
      } else {
        count -= length;
      }
    }

    // If we don't run out of characters, go all the way to the beginning of the
    // next node.
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return {
    node: parent.nextSibling,
    offset: 0
  };
}

document.onmouseup = updateSelection;
document.onkeyup = updateSelection;

/***/ })
/******/ ]);