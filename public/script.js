const textElement = document.getElementById('text');
const documentText = textElement.innerText;
// Remove leading whitespace from the node.
textElement.innerHTML = textElement.innerText;

function updateSelection() {
  const selection = window.getSelection();

  if (selection.rangeCount == 0) {
    return;
  }

  const range = selection.getRangeAt(0);
  const { startOffset, endOffset } = getOffsetsRelativeToParent(range, textElement);
  const selectedText = selection.toString();

  const highlighted = highlightWords(documentText, selectedText);

  textElement.innerHTML = highlighted;

  // Setting innerHTML removes the selection. Restore it.
  selection.removeAllRanges();
  selection.addRange(getParentRangeFromOffsets(textElement, startOffset, endOffset));
}

function highlightWords(text, word) {
  // Make the highlighting case-insensitive.
  const lowerText = text.toLowerCase();
  word = word.toLowerCase();

  if (word.length == 0) {
    return text;
  }

  let highlighted = '';
  let pos = 0;

  while (pos < text.length) {
    let next = lowerText.indexOf(word, pos);

    if (next == -1) {
      highlighted += text.substr(pos);
      break;
    }

    highlighted += text.substr(pos, next - pos);
    highlighted += '<span class="highlighted">' + text.substr(next, word.length) + '<\/span>';
    pos = next + word.length;
  }

  return highlighted
}

function getOffsetsRelativeToParent(range, parent) {
  const rangeBefore = document.createRange();
  rangeBefore.selectNodeContents(parent);
  rangeBefore.setEnd(range.startContainer, range.startOffset);
  const startOffset = rangeBefore.toString().length;
  const endOffset = startOffset + range.toString().length;

  return { startOffset, endOffset };
}

function getParentRangeFromOffsets(parent, startOffset, endOffset) {
  const range = document.createRange();
  const start = getNodeAndOffsetByParentOffset(parent, startOffset);
  const end = getNodeAndOffsetByParentOffset(parent, endOffset);
  range.setStart(start.node, start.offset);
  range.setEnd(end.node, end.offset);

  return range;
}

function getNodeAndOffsetByParentOffset(parent, offset) {
  let count = offset;

  // Count the number of characters in each child node until we find the node
  // corresponding to our offset.
  for (let child of parent.childNodes) {
    const length = child.textContent.length;

    if (count < length) {
      return {
        node: child,
        offset: count
      };
    } else {
      count -= length;
    }
  }

  // If we don't run out of characters, go all the way to the beginning of the
  // next node.
  return {
    node: parent.nextSibling,
    offset: 0
  };
}

document.onmouseup = updateSelection;
document.onkeyup = updateSelection;
