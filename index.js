const server = require('./src/server');

const port = process.env.PORT || 3000;

server.listen(port, (err) => {
  if (err) {
    console.log('Server failed to start.', err.stack);
    return;
  }

  console.log(`server is listening on ${port}`)
});
